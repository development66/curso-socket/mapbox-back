import { Router, Request, Response } from 'express';
import Server from '../classes/server';
import { GraficaData } from '../classes/grafica';
import { mapa } from '../sockets/sockets';

export const router = Router();

router.get('/mapa', (req: Request, res: Response) => {
    res.json(mapa.getMarcadores());
});


const grafica = new GraficaData();


router.post('/grafica', (req: Request, res: Response) => {
    const mes = req.body.mes;
    const unidades = Number(req.body.unidades);
    grafica.incrementarValor(mes, unidades);

    const server = Server.instance;
    server.io.emit('cambio-grafica', grafica.getDataGrafica());

    res.json(grafica.getDataGrafica());
});

router.post('/mensajes/:id', (req: Request, res: Response) => {
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;
    const id = req.params.id;
    const payload = {
        de,
        cuerpo
    };

    const server = Server.instance;
    server.io.in(id).emit('mensaje-privado', payload)

    res.json({
        ok: true,
        cuerpo,
        de,
        id
    });
});